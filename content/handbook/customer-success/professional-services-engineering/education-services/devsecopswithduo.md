---
title: "DevSecOps with GitLab Duo - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises used in the DevSecOps with GitLab Duo course."
---

# DevSecOps with GitLab Duo

## Lab Guides

 Lab Guide | Link 
-----------|------------
 Lab 1: Getting Started with GitLab Duo| [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab1.md)
 Lab 2: Code Generation with GitLab Duo| [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab2.md)
 Lab 3: Working with Issues and Merge Requests| [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab3.md)
 Lab 4: Using GitLab Duo to Write New Code| [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab4.md)
 Lab 5: Working with Security Vulnerabilities| [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab5.md)


## Quick links

* DevSecOps with GitLab Duo Course Description

## Suggestions?

If you’d like to suggest changes to the *DevSecOps With GitLab Duo*, please submit them via merge request.
